import { Injectable } from '@angular/core';
import {AngularFireAuth} from "angularfire2/auth";
import {AlertProvider} from "../alert/alert";
import {StateProvider} from "../state/state";
import firebase from 'firebase';

@Injectable()
export class AuthProvider {
  constructor(
    private afa: AngularFireAuth,
    private state: StateProvider,
    private alert: AlertProvider
  ) {
    console.log('Hello AuthProvider Provider');
  }

  async login(email: string, password: string) {
    try {
      await this.afa.auth.signInWithEmailAndPassword(email, password);
    } catch(error) {
      this.handleError(error);
    }
  }

  async signup(email: string, password: string, signupData?: any) {
    // phoneNumber, photoUrl
    console.log('signupData', signupData);
    let photoURL = '';
    try {
      this.state.currentUser = await this.afa.auth.createUserWithEmailAndPassword(email, password);
      if (signupData.photoURL) {
        console.log('image selected', signupData.photoURL)
        photoURL = await this.uploadProfilePhoto(signupData.photoURL);
        console.log('uploaded photo', photoURL);
      }
      await this.afa.auth.currentUser.updateProfile({
        displayName: signupData.displayName,
        photoURL
      });
      this.state.currentUser = this.afa.auth.currentUser;
    } catch (error) {
      this.handleError(error);
    }
  }

  getAuthState() {
    this.afa.authState.subscribe(user => {
      console.log(user);
      if (user) {
        this.state.currentUser = user;
      } else {
        this.state.currentUser = null;
      }
    }, () => {

    });
  }

  async logout() {
    try {
      await this.afa.auth.signOut();
      this.state.currentUser = null;
    } catch (error) {
      this.handleError(error);
    }
  }

  async uploadProfilePhoto(image: string) {
    console.log(image);
    let storageRef = firebase.storage().ref().child(this.state.currentUser.uid + '/profile_photo/');
    console.log('storage ref', storageRef);
    const savedImage = await storageRef.putString(image, 'base64', { contentType: 'image/jpg' });
    console.log('saved image', savedImage);
    return savedImage.downloadURL;
  }

  async forgotPassword(email: string) {
    try {
      await this.afa.auth.sendPasswordResetEmail(email);
    } catch (error) {
      this.handleError(error);
    }
  }

  handleError(error) {
    this.state.firebaseError = true;
    this.state.firebaseErrorMessage = error.message;
  }
}
