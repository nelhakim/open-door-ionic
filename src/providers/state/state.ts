import { Injectable } from '@angular/core';

@Injectable()
export class StateProvider {
  public currentUser;
  public firebaseErrorMessage = '';
  public firebaseError: boolean;
  public defaultProfilePic = './assets/avatar.png';
  constructor() {
    console.log('Hello StateProvider Provider');
  }

  clearFirebaseErrors() {
    this.firebaseError = false;
    this.firebaseErrorMessage = '';
  }

}
