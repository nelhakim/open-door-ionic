import { NgModule } from '@angular/core';
import { Base64Pipe } from './base64/base64';
//import { CurencyPipe } from './curency/curency';
@NgModule({
    declarations: [Base64Pipe
        //CurencyPipe
    ],
    imports: [],
    exports: [Base64Pipe
        //CurencyPipe
    ]
})
export class PipesModule { }
