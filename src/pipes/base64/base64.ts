import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'base64',
})
export class Base64Pipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string,) {
    console.log("data:image/jpeg;base64," + value);
    return "data:image/jpeg;base64," + value;
  }
}
