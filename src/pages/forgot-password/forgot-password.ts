import {Component, OnDestroy} from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { LoadingProvider } from '../../providers/loading/loading';
import { Http } from '@angular/http';
import { ConfigProvider } from '../../providers/config/config';
import {AuthProvider} from "../../providers/auth/auth";
import {StateProvider} from "../../providers/state/state";

@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage implements OnDestroy {
  formData = {
    customers_email_address: '',
  };
  errorMessage = '';
  passwordSent: boolean;
  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    public loading: LoadingProvider,
    public http: Http,
    private auth: AuthProvider,
    public state: StateProvider,
    public config: ConfigProvider,
    public navParams: NavParams) {
  }
  async forgetPassword() {
    this.loading.show();
    await this.auth.forgotPassword(this.formData.customers_email_address);
    this.loading.hide();
    if (!this.state.firebaseError) {
      this.passwordSent = true;
    } else {
      this.passwordSent = false;
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  ngOnDestroy() {
    this.state.clearFirebaseErrors();
  }

}
