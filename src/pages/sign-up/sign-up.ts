import { Component } from '@angular/core';
import { ViewController, ModalController } from 'ionic-angular';
import { LoadingProvider } from '../../providers/loading/loading';
import { ConfigProvider } from '../../providers/config/config';
import { Http } from '@angular/http';
import { SharedDataProvider } from '../../providers/shared-data/shared-data';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Platform } from 'ionic-angular';
import { LoginPage } from '../login/login';
import {AuthProvider} from "../../providers/auth/auth";
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {
  formData = {
    customers_firstname: '',
    customers_lastname: '',
    customers_email_address: '',
    customers_password: '',
    customers_telephone: '',
    customers_picture: ''
  };
  cameraOptions: CameraOptions = {
    quality: 80,
    sourceType: this.camera.PictureSourceType.CAMERA,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    allowEdit: true,
    targetWidth: 150,
    targetHeight: 150,
    saveToPhotoAlbum: false,
    correctOrientation: true
  }
  image;
  errorMessage = '';
  constructor(
    public http: Http,
    public config: ConfigProvider,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public loading: LoadingProvider,
    public shared: SharedDataProvider,
    public platform: Platform,
    private auth: AuthProvider,
    private camera: Camera
  ) {
  }
  async openCamera() {
    try {
      this.image = await this.camera.getPicture(this.cameraOptions);
      return this.image;
    } catch (error) {
      console.log(error);
    }
  }

  async openGallery() {
    this.cameraOptions.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
    try {
      this.image = await this.camera.getPicture(this.cameraOptions);
      return this.image;
    } catch (error) {
      console.log(error);
    }
  }
  signUp() {
    this.loading.show();
    this.errorMessage = '';
    // this.formData.customers_picture = this.image;
    const signupData: any = {
      displayName: this.formData.customers_firstname + ' ' + this.formData.customers_lastname,
      phoneNumber: this.formData.customers_telephone
    };
    if(this.image) {
      signupData.photoURL = this.image;
    }
    this.auth.signup(this.formData.customers_email_address, this.formData.customers_password, signupData);
    this.loading.hide();
    this.viewCtrl.dismiss();
  }

  openPrivacyPolicyPage() {
    let modal = this.modalCtrl.create('PrivacyPolicyPage');
    modal.present();
  }
  openTermServicesPage() {
    let modal = this.modalCtrl.create('TermServicesPage');
    modal.present();
  }
  openRefundPolicyPage() {
    let modal = this.modalCtrl.create('RefundPolicyPage');
    modal.present();
  }
  dismiss() {
    this.viewCtrl.dismiss();
    // let modal = this.modalCtrl.create(LoginPage);
    // modal.present();
  }
  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad SignUpPage');
  // }
}
